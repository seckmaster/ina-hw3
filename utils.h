#include <iostream>
#include <Graph.h>
#include <igraph_wrapper.h>
#include <functional>
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <optional>

using namespace std;

igraph_vector_t to_igraph_vector(const vector<int> &vec) {
    igraph_vector_t res;
    igraph_vector_init(&res, 0);
    for (auto it = vec.begin(); it != vec.end(); it++) {
        igraph_vector_push_back(&res, *it);
    }
    return res;
}

float compare_communities(const vector<int> &comm1, 
                          const vector<int> &comm2,
                          igraph_community_comparison_t method) {
    assert(comm1.size() == comm2.size());

    auto _comm1 = to_igraph_vector(comm1);
    auto _comm2 = to_igraph_vector(comm2);
    assert(igraph_vector_size(&_comm1) == igraph_vector_size(&_comm2));

    igraph_real_t result;
    igraph_compare_communities(&_comm1, &_comm2, &result, method);
    return result;
}

igraph_wrapper generate_girvan_newman_graph_igraph(uint groups, 
                                                   uint n, 
                                                   uint expected_degree,
                                                   float mixing,
                                                   int seed) {
    srand(seed);
    
    auto graph = igraph_wrapper((igraph_integer_t) groups * n, IGRAPH_UNDIRECTED);
    
    auto p_community = static_cast<float>(expected_degree) / static_cast<float>(n) * (1 - mixing);
    auto p_between   = static_cast<float>(expected_degree) / static_cast<float>(groups * n - n) * mixing;

    auto community = [n](uint node) { return node / n; };

    for (int i = 0; i < graph.vertices_count(); i++) {
        auto community_i = community(i);
        for (int j = i + 1; j < graph.vertices_count(); j++) {
            auto community_j = community(j);
            float perc;
            if (community_i == community_j) {
                perc = p_community;
            } else {
                perc = p_between;
            }

            auto rnd = ((float) rand() / (RAND_MAX));
            if (rnd < perc) {
                graph.add_edge(i, j);
            }
        }
    }

    return graph;
}

int number_of_communities(const vector<int> &communities) {
    auto set = unordered_set<int>();
    for (auto it = communities.begin(); it != communities.end(); it++) {
        set.insert(*it);
    }
    return set.size();
}

typedef function<vector<int>(const igraph_wrapper&)> CommunityDetectionAlgorithm;

vector<vector<float>> benchmark_community_detection(const vector<CommunityDetectionAlgorithm> &algorithms,
                                                    const vector<vector<igraph_wrapper>> &graphs,
                                                    const vector<vector<vector<int>>> &correct_communities,
                                                    igraph_community_comparison_t method,
                                                    bool normalize,
                                                    bool verbose) {
    vector<vector<float>> benchmarks;
    benchmarks.reserve(algorithms.size());
    for (int i = 0; i < algorithms.size(); i++) {
        auto vec = vector<float>(graphs.size());
        benchmarks.push_back(vec);
    }

    for (int alg = 0; alg < algorithms.size(); alg++) {
        auto &algorithm = algorithms[alg];
        if (verbose) 
            cout << "Alg: " << alg + 1 << " / " << algorithms.size() << endl;
        for (int g = 0; g < graphs.size(); g++) {
            if (verbose) 
                cout << "Graphs: " << g + 1 << " / " << graphs.size() << endl;
            for (int gr = 0; gr < graphs[g].size(); gr++) {
                if (verbose) 
                    cout << "Graph: " << gr + 1 << " / " << graphs[g].size() << endl;
                auto &graph      = graphs[g][gr];
                auto communities = algorithm(graph);
                auto &correct    = correct_communities[g][gr];
                auto score       = compare_communities(correct, communities, method);
                if (normalize) {
                    score /= log2f(static_cast<float>(graph.vertices_count()));
                }
                benchmarks[alg][g] += score;
            }
        }
    }

    int i = 0;
    for (auto it = benchmarks.begin(); it != benchmarks.end(); it++) {
        for (auto it2 = it->begin(); it2 != it->end(); it2++) {
            *it2 /= (float) graphs[0].size();
        }
        i++;
    }

    return benchmarks;
} 

vector<int> load_communities_pajek(const string &file) {
    std::ifstream infile(file);
    std::string line;
    std::getline(infile, line);

    auto search_str = std::string("*vertices ");
    auto index = line.find(search_str);
    if (index == std::string::npos) {
        cerr << "Invalid file format" << endl;
        exit(10);
    }
    
    auto nodes_count = stoi(line.substr(index + search_str.size()));
    
    vector<int> communities;
    communities.reserve(nodes_count);

    for (int i = 0; i < nodes_count; i++) {
        getline(infile, line);

        auto pos = line.find('"');
        if (pos == string::npos) {
            cerr << "Invalid file format" << endl;
            exit(10);
        }
        line = line.substr(pos + 1);
        pos = line.find('"');
        if (pos == string::npos) {
            cerr << "Invalid file format" << endl;
            exit(10);
        }

        auto c  = line.substr(pos + 1);
        auto ci = stoi(c);
        communities.push_back(ci - 1);
    }

    return communities;
}

vector<pair<int, pair<int, int>>> load_years_and_communities_pajek(const string &file) {
    std::ifstream infile(file);
    std::string line;
    std::getline(infile, line);

    auto search_str = std::string("*vertices ");
    auto index = line.find(search_str);
    if (index == std::string::npos) {
        cerr << "Invalid file format" << endl;
        exit(10);
    }
    
    auto nodes_count = std::stoi(line.substr(index + search_str.size()));
    
    vector<pair<int, pair<int, int>>> items;
    items.reserve(nodes_count);

    for (int i = 0; i < nodes_count; i++) {
        getline(infile, line);

        auto pos = line.find('"');
        if (pos == string::npos) {
            cerr << "Invalid file format" << endl;
            exit(10);
        }
        line = line.substr(pos + 1);
        pos = line.find('"');
        if (pos == string::npos) {
            cerr << "Invalid file format" << endl;
            exit(10);
        }
        
        auto year = stoi(line.substr(pos - 4, 4));

        auto c = line.substr(pos + 1);
        auto community = stoi(c);
        items.push_back(make_pair(i, make_pair(year, community - 1)));
    }

    return items;
}

template <typename T>
vector<T> intersection(const vector<T> &v1, const vector<T> &v2) {
    const vector<T> *shorter;
    const vector<T> *longer;
    vector<T> intersect;

    if (v1.size() < v2.size()) {
        shorter = &v1;
        longer  = &v2;
        intersect.reserve(v1.size());
    } else {
        shorter = &v2;
        longer  = &v1;
        intersect.reserve(v2.size());
    }

    for (auto it = shorter->begin(); it != shorter->end(); it++) {
        if (find(longer->begin(), longer->end(), *it) == longer->end()) continue;
        intersect.push_back(*it);
    }
    return intersect;
}

float adamicAdarIndex(const igraph_wrapper &graph, int from, int to) {
    auto neighbours_from = graph.neighbours_non_owned(from, IGRAPH_ALL);
    auto neighbours_to   = graph.neighbours_non_owned(to, IGRAPH_ALL);
    auto intersect       = intersection(neighbours_from, neighbours_to);
    auto index = 0.0f;
    for (auto it = intersect.begin(); it != intersect.end(); it++) {
        index += 1.0f / logf(static_cast<float>(graph.degree(*it)));
    }
    return index;
}

int C(int n, int k) {
    double res = 1;
    for (int i = 1; i <= k; ++i)
        res = res * (n - k + i) / i;
    return (int)(res + 0.01);
}

/// DONT FORGET TO CLEAR EVERY TIME 
/// YOU SWITCH THE GRAPH!!!!!!!
struct Cache {
    vector<int> communities;
    unordered_map<int, vector<int>> community_nodes_mapping;
    unordered_map<int, int>         community_edges_count_mapping;
};
optional<Cache> cache;

float communityIndexLouvain(const igraph_wrapper &graph, int from, int to) {
    vector<int> communites;
    unordered_map<int, vector<int>> community_nodes_mapping;
    unordered_map<int, int>         community_edges_count_mapping;

    if (cache.has_value()) {
        communites                    = cache->communities;
        community_nodes_mapping       = cache->community_nodes_mapping;
        community_edges_count_mapping = cache->community_edges_count_mapping;       
    } else {
        /// @NOTE: Perform preprocessing so that its faster
        /// in the future

        communites = graph.community_louvain_non_owned();
        int node = 0;
        for (auto it = communites.begin(); it != communites.end(); it++, node++) {
            if (!community_nodes_mapping.contains(*it)) {
                community_nodes_mapping[*it] = vector<int>();
            }
            community_nodes_mapping[*it].push_back(node);
        }
        for (auto community = community_nodes_mapping.begin(); community != community_nodes_mapping.end(); community++) {
            for (auto it = community->second.begin(); it != community->second.end(); it++) {
                for (auto it2 = it + 1; it2 != community->second.end(); it2++) {
                    assert(*it != *it2);
                    if (community_edges_count_mapping.contains(community->first)) {
                        community_edges_count_mapping[community->first] += graph.are_nodes_connected(*it, *it2, false);
                    } else {
                        community_edges_count_mapping[community->first]  = graph.are_nodes_connected(*it, *it2, false);
                    }
                }
            }
        }
        cache = { communites, community_nodes_mapping, community_edges_count_mapping };
    }

    auto community_from = -1;
    auto community_to   = -1;

    int i = 0;
    for (auto it = communites.begin(); it != communites.end(); it++, i++) {
        if (i == from) community_from = *it;
        if (i == to)   community_to   = *it;
        if (community_from >= 0 && community_to >= 0) break;
    }

    if (community_from != community_to) return 0.0f;
    
    auto nc = community_nodes_mapping[community_from].size();
    auto mc = community_edges_count_mapping[community_from];

    float score = static_cast<float>(mc) / C(nc, 2);
    assert(!isnan(score));
    return score;
}

inline bool are_equal(float a, float b, float eps = 10e-9) {
    return abs(a - b) <= eps;
}

typedef function<igraph_wrapper()> GraphProvider;
typedef function<float(const igraph_wrapper&, int from, int to)> LinkPredictionMethod;

float link_prediction_framework(igraph_wrapper graph, 
                                const LinkPredictionMethod &method, 
                                bool verbose, 
                                int seed) {
    srand(seed);
    
    auto m       = graph.edges_count();
    auto m_tenth = m / 10;

    ///

    igraph_vector_t edges;
    igraph_vector_init(&edges, 0);
    igraph_edges(&graph.graph, igraph_ess_all(IGRAPH_EDGEORDER_FROM), &edges);

    vector<pair<int, int>> edges_vec; // Lp will be here
    edges_vec.reserve(igraph_vector_size(&edges));
    for (int i = 0; i < igraph_vector_size(&edges) / 2; i += 2) {
        auto from = VECTOR(edges)[i];
        auto to   = VECTOR(edges)[i + 1];
        edges_vec.push_back(make_pair(from, to));
    }
    igraph_vector_destroy(&edges);
    
    if (verbose)
        cout << "Edges list ..." << endl;

    /// Ln
    
    vector<pair<int, int>> Ln;
    Ln.reserve(m_tenth);
    while (Ln.size() < m_tenth) {
        auto from = rand() % graph.vertices_count();
        auto to   = rand() % graph.vertices_count();
        if (from == to) continue;
        if (graph.are_nodes_connected(from, to, false)) continue;
        Ln.push_back(make_pair(from, to));

        if (verbose && Ln.size() % 1 == 0) {
            cout << "Ln: " << Ln.size() << " / " << m_tenth << "\r";
            flush(cout);
        }
    }

    /// Lp

    random_device rd;
    mt19937 g(rd());
    g.seed(seed);
    shuffle(edges_vec.begin(), edges_vec.end(), g);

    igraph_vector_init(&edges, 0);
    for (int i = 0; i < m_tenth; i++) {
        auto edge    = edges_vec[i];
        auto edge_id = graph.edge_id(edge.first, edge.second, false);
        igraph_vector_push_back(&edges, edge_id);
    }
    
    auto res = igraph_delete_edges(&graph.graph, igraph_ess_vector(&edges));
    if (verbose) {
        cout << "Delete edges result: " << res << endl;
    }

    if (igraph_vector_size(&edges) != m_tenth) {
        cerr << "Size is not correct !!" << endl;
        exit(321);
    }

    if(graph.edges_count() != m - m_tenth) {
        cerr << "Not enough nodes removed !?" << endl;
        cout << igraph_vector_size(&edges) << ", " << m << ", " << m_tenth << ", " << graph.edges_count() << endl;
        exit(321);
    }
    igraph_vector_destroy(&edges);

    /// AUC

    int count1 = 0; // m'
    int count2 = 0; // m''

    for (int i = 0; i < m_tenth; i++) {
        auto a = rand() % m_tenth;
        auto b = rand() % m_tenth;

        auto from_lp = edges_vec[a];
        auto from_ln = Ln[b];

        auto score_lp = method(graph, from_lp.first, from_lp.second);
        auto score_ln = method(graph, from_ln.first, from_ln.second);

        if (are_equal(score_lp, score_ln)) {
            count2++;
        } else if (score_lp > score_ln) {
            count1++;
        }

        if (verbose) {
            cout << "AUC: " << i << " / " << m_tenth << "\r";
            flush(cout);
        }
    }

    // cout << count1 << ", " << count2 << endl;

    float auc = (static_cast<float>(count1) + static_cast<float>(count2) * 0.5f) / static_cast<float>(m_tenth);
    return auc;
}

template<typename T>
vector<T> filter(const vector<T> &vec, function<bool(const T&)> predicate) {
    auto filtered = vector<T>();
    copy_if(vec.begin(), vec.end(), 
            back_inserter(filtered), 
            predicate);
    return filtered;
}

template<typename T, typename U>
vector<U> fmap(const vector<T> &vec, function<U(const T&)> map_to) {
    auto mapped = vector<U>();
    transform(vec.begin(), vec.end(), 
              back_inserter(mapped), 
              map_to);
    return mapped;
}
