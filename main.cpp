#include <iostream>
#include <Graph.h>
#include <igraph_wrapper.h>
#include <random>
#include <GraphUtils.h>
#include <vector>
#include <unordered_set>
#include <algorithm>
#include "matplotlibcpp.h"
#include "utils.h"

using namespace std;

void exercise_2() {
    auto Q = [](float n, float nc) {
        return (n / nc) * ((nc - 1) / n - pow(nc / n, 2));
    };

    for (int n = 8; n <= 2048; n *= 2) {
        // int n = 1000;

        float best = -1;
        int best_nc = -1;

        for (int i = 1; i <= n; i++) {
            auto q = Q(n, i);
            if (q > best) {
                best = q;
                best_nc = i;
            }
            // cout << i << ": " << q << endl;
        }
        
        cout << n << ":" << endl;
        cout << best_nc << ": " << best << endl;

        auto predicted = sqrt(n);
        cout << round(predicted) << ": " << Q(n, predicted) << endl;
        cout << endl;
    }
}

void exercise_3_i(const vector<CommunityDetectionAlgorithm> &algorithms,
                  const vector<string> &names,
                  const vector<string> &colors) {
    namespace plt = matplotlibcpp;

    auto community_size   = 24;
    auto communites_count = 3;
    auto nodes_count      = community_size * communites_count;

    vector<float> mixing_params = { 0, 0.1f, 0.2f, 0.3f, 0.4f, 0.5f };
    vector<int> real_community;
    for (int i = 0; i < nodes_count; i++) {
        real_community.push_back(i / community_size);
    }

    auto num_graphs = 25;
    auto graphs = vector<vector<igraph_wrapper>>();
    auto real_communities = vector<vector<vector<int>>>();

    for (auto it = mixing_params.begin(); it != mixing_params.end(); it++) {
        auto vec = vector<igraph_wrapper>();
        auto comm_vec = vector<vector<int>>();
        for (int i = 0; i < num_graphs; i++) {
            auto graph = generate_girvan_newman_graph_igraph(communites_count, community_size, 20, *it, i * 300 + 2314728);
            vec.push_back(graph);
            comm_vec.push_back(real_community);
        }
        graphs.push_back(vec);
        real_communities.push_back(comm_vec);
    }

    auto benchmark = benchmark_community_detection(algorithms,
                                                   graphs,
                                                   real_communities,
                                                   IGRAPH_COMMCMP_NMI,
                                                   false,
                                                   false);


    for (int i = 0; i < algorithms.size(); i++) {
        plt::named_plot(names[i], mixing_params, benchmark[i], colors[i]);
    }

    plt::legend();
    plt::show();
}

void exercise_3_ii(const vector<CommunityDetectionAlgorithm> &algorithms,
                   const vector<string> &names,
                   const vector<string> &colors) {
    namespace plt = matplotlibcpp;
    
    vector<string> percentages  = { "_00_", "_02_", "_04_", "_06_", "_08_" };
    vector<float> mixing_params = { 0, 0.2f, 0.4f, 0.6f, 0.8f };
    
    auto num_graphs       = 25;
    auto graphs           = vector<vector<igraph_wrapper>>();
    auto real_communities = vector<vector<vector<int>>>();
    
    for (auto it = percentages.begin(); it != percentages.end(); it++) {
        auto file_prefix = "../data/LFR/LFR" + *it;
        auto vec = vector<igraph_wrapper>();
        auto comm_vec = vector<vector<int>>();

        for (int i = 0; i < num_graphs; i++) {
            auto file = file_prefix + to_string(i) + ".adj";
            
            auto graph = igraph_wrapper::read_graph_edgelist(file, false);
            auto real_communities = load_communities_pajek(file_prefix + to_string(i) + ".net");

            vec.push_back(graph);
            comm_vec.push_back(real_communities);
        }
        graphs.push_back(vec);
        real_communities.push_back(comm_vec);
    }

    cout << "Loaded all graphs!" << endl;
    auto benchmark = benchmark_community_detection(algorithms,
                                                   graphs,
                                                   real_communities,
                                                   IGRAPH_COMMCMP_NMI,
                                                   false,
                                                   false);

    for (int i = 0; i < algorithms.size(); i++) {
        plt::named_plot(names[i], mixing_params, benchmark[i], colors[i]);
    }

    plt::legend();
    plt::show();
}

void exercise_3_iii(const vector<CommunityDetectionAlgorithm> &algorithms,
                    const vector<string> &names,
                    const vector<string> &colors) {
    namespace plt = matplotlibcpp;

    int num_graphs = 25;
    int graph_size = 1000;
    
    vector<float> degrees = { 8, 16, 24, 32, 40 };

    auto graphs           = vector<vector<igraph_wrapper>>();
    auto real_communities = vector<vector<vector<int>>>();

    for (int k = 0; k < degrees.size(); k++) {
        auto vec = vector<igraph_wrapper>();
        auto comm_vec = vector<vector<int>>();

        for (int g = 0; g < num_graphs; g++) {
            auto graph = igraph_wrapper::erdos_renyi(
                graph_size, graph_size * degrees[k] * 0.5, IGRAPH_UNDIRECTED,
                igraph_rng_default(), time(nullptr));
            auto real_communities = vector<int>(graph.vertices_count(), -1);

            auto cc = graph.get_weak_components();
            int current_community = 0;
            for (auto it = cc.begin(); it != cc.end(); it++, current_community++) {
                for (auto it2 = it->begin(); it2 != it->end(); it2++) {
                    real_communities[*it2] = current_community;
                }
            }
            comm_vec.push_back(real_communities);
            vec.push_back(graph);
        }
        graphs.push_back(vec);
        real_communities.push_back(comm_vec);
    }

    auto benchmark = benchmark_community_detection(algorithms,
                                                   graphs,
                                                   real_communities,
                                                   IGRAPH_COMMCMP_VI,
                                                   true,
                                                   false);

    for (int i = 0; i < algorithms.size(); i++) {
        plt::named_plot(names[i], degrees, benchmark[i], colors[i]);
    }

    plt::legend();
    plt::show();
}

void exercise_3_iv(const vector<CommunityDetectionAlgorithm> &algorithms) {
    namespace plt = matplotlibcpp;

    auto graph             = igraph_wrapper::read_graph_edgelist("../data/dolphins.adj", false);
    auto real_communities  = load_communities_pajek("../data/dolphins.net");

    auto repeat = 25;
    vector<float> results(algorithms.size(), 0.0f);

    int i = 0;
    for (auto alg = algorithms.begin(); alg != algorithms.end(); alg++, i++) {
        vector<vector<int>> all_detected_communities;
        all_detected_communities.reserve(repeat);
        for (int i = 0; i < repeat; i++) {
            auto detected_communities = (*alg)(graph);
            all_detected_communities.push_back(detected_communities);
        }

        /// compute pair-wise NVI
        int c = 0;
        for (auto it1 = all_detected_communities.begin(); it1 != all_detected_communities.end(); it1++) {
            for (auto it2 = it1 + 1; it2 != all_detected_communities.end(); it2++) {
                auto score = compare_communities(*it1, *it2, IGRAPH_COMMCMP_VI);
                score /= log2f(static_cast<float>(it1->size()));
                results[i] += score;
                c++;
            }   
        }
        assert(c == all_detected_communities.size() * (all_detected_communities.size() - 1) / 2);
        results[i] /= static_cast<float>(all_detected_communities.size() * all_detected_communities.size() / 2);
    }

    for (auto it = results.begin(); it != results.end(); it++) {
        cout << *it << endl;
    }
}

void exercise_3() {
    const vector<CommunityDetectionAlgorithm> algorithms = {
        [](auto graph) { return graph.community_label_propagation_non_owned(); },
        [](auto graph) { return graph.community_louvain_non_owned(); },
        [](auto graph) { return graph.community_infomap_non_owned(1); },
    };
    const vector<string> algorithm_names = {
        "label prop", "Louvain", "infomap"
    };

    const vector<string> colors = {
        ".-r", ".-g", ".-b"
    };
    exercise_3_i  (algorithms, algorithm_names, colors);
    exercise_3_ii (algorithms, algorithm_names, colors);
    exercise_3_iii(algorithms, algorithm_names, colors);
    exercise_3_iv (algorithms);
}

float link_prediction_benchmark(int repeat, const GraphProvider &graph, const LinkPredictionMethod &method, bool verbose) {
    auto acc = 0.0f;    
    for (int i = 0; i < repeat; i++) {
        auto auc = link_prediction_framework(graph(), method, verbose, time(nullptr));
        acc += auc;
    }
    return acc / static_cast<float>(repeat);
}

void exercise_4() {
    const vector<LinkPredictionMethod> methods = {
        // 1. Preferential attachment index
        [](auto &graph, auto from, auto to) { 
            return graph.degree(to);
        },
        // 2. Adamic-Adar index
        // adamicAdarIndex,
        // 3. Community index [Louvain] 
        // communityIndexLouvain,
    };
    const vector<GraphProvider> graphs = {
        /**
        Nodes    | 25000
        Edges    | 250000
        <k>      | 20
        <C>      | 0.000828799
        Diameter | 5
        <d>      | 3.70723
         */
        []() { return igraph_wrapper::erdos_renyi(25000, 250000, IGRAPH_UNDIRECTED, igraph_rng_default(), time(nullptr)); },
        /**
        Nodes    | 4039
        Edges    | 88234
        <k>      | 43.691
        <C>      | 0.519174
        Diameter | 8
        <d>      | 3.69251
         */
        []() { return igraph_wrapper::read_graph_edgelist("../data/circles.adj", false); },
        /**
        Nodes    | 62586
        Edges    | 147892
        <k>      | 4.72604
        <C>      | 0.00387202
        Diameter | 11
        <d>      | 5.93552
         */
        []() { return igraph_wrapper::read_graph_edgelist("../data/gnutella.adj", false); },
        /**
        Nodes    | 75885
        Edges    | 357317
        <k>      | 9.41733
        <C>      | 0.00136163
        Diameter | 23
        <d>      | 5.83227
         */
        []() { return igraph_wrapper::read_graph_edgelist("../data/nec.adj", false); },
    };

    // for (auto it = graphs.begin(); it != graphs.end(); it++) {
    //     print_graph_statistics(*it, "");
    // }
    // namespace plt = matplotlibcpp;
    // {
    //     auto g = Graph::read_edges_list("../data/gnutella.adj", 62586, 147892, false).value();
    //     auto degree_distr = probability_distribution(g.get_degree_distribution());
    //     plt::named_loglog("Gnutella degree distribution", degree_distr.first, degree_distr.second, ".m");
    //     g = Graph::read_edges_list("../data/nec.adj", 75885, 357317, false).value();
    //     degree_distr = probability_distribution(g.get_degree_distribution());
    //     plt::named_loglog("Internet degree distribution", degree_distr.first, degree_distr.second, ".b");
    //     plt::legend();
    //     plt::show();
    //     return;
    // }

    auto repeat_benchmarks = 1;
    for (auto git = graphs.begin(); git != graphs.end(); git++) {
        for (auto it = methods.begin(); it != methods.end(); it++) {
            auto auc = link_prediction_benchmark(repeat_benchmarks, *git, *it, false);
            cout << "AUC: " << auc << endl;
        }
        cache = {};
        cout << endl;
    }

    /**
    RESULTS:
    ER
    0.42258
    0.499164
    0.496314
    
    Circles
    0.832251
    0.991681
    0.939969

    Gnutella
    0.760937
    0.511705
    0.689709

    Nec
    0.956948
    0.645812
    0.921483
    */
}

float CA(const vector<int> &real, const vector<int> &predicted) {
    assert(real.size() == predicted.size());

    auto correct = 0;
    auto it1 = real.begin();
    auto it2 = predicted.begin();
    for (; it1 != real.end(); it1++, it2++) {
        correct += *it1 == *it2;
    }
    return static_cast<float>(correct) / static_cast<float>(real.size());
}

vector<int> predict_baseline(const igraph_wrapper &graph,
                             const vector<int> &real_commnunities,
                             const vector<int> &predict) {
    auto result = vector<int>();
    result.reserve(predict.size());
    for (auto it = predict.begin(); it != predict.end(); it++) {
        auto counter = unordered_map<int, int>();
        auto neighbourhood = graph.neighbours_non_owned(*it, IGRAPH_ALL);
        for (auto it2 = neighbourhood.begin(); it2 != neighbourhood.end(); it2++) {
            auto community = real_commnunities[*it2];
            if (counter.contains(community)) {
                counter[community]++;
            } else {
                counter[community] = 1;
            }
        }
        auto most_frequent = counter.begin();
        for (auto it2 = counter.begin(); it2 != counter.end(); it2++) {
            if (it2->second > most_frequent->second) {
                most_frequent = it2;
            }
        }
        result.push_back(most_frequent->first);
    }
    return result;
}

// predict using some community detection scheme
vector<int> predict_A(const igraph_wrapper &graph,
                      const vector<int> &real_commnunities,
                      const vector<int> &predict) {
    auto result = vector<int>();
    result.reserve(predict.size());
    auto communities = graph.community_louvain_non_owned();
    for (auto it = predict.begin(); it != predict.end(); it++) {
        auto community = communities[*it];
        result.push_back(community);
    }
    return result;
}

// predict using assortativeness;
// more precisely, locate a neighbour or a neighbour's neighbour 
// with the most similar degree in use its journal as prediction
vector<int> predict_B(const igraph_wrapper &graph,
                      const vector<int> &real_commnunities,
                      const vector<int> &years,
                      const vector<int> &predict) {
    auto vec = to_igraph_vector(real_commnunities);
    igraph_real_t res;
    igraph_assortativity_nominal(&graph.graph, &vec, &res, false);
    // cout << res << endl; 
    // cout << res << endl; 
    // @NOTE: we observe a high assortativeness, as one would expect

    auto result = vector<int>();
    result.reserve(predict.size());
    for (auto it = predict.begin(); it != predict.end(); it++) {
        auto my_degree = graph.degree(*it);

        auto most_similar = -1;
        auto most_similar_degree = -1;

        auto visited = unordered_set<int>();
        auto queue = vector<int>();

        {
            auto neighbourhood = graph.neighbours_non_owned(*it, IGRAPH_ALL);
            for (auto it2 = neighbourhood.begin(); it2 != neighbourhood.end(); it2++) {
                queue.push_back(*it2);
                visited.insert(*it2);
            }
        }

        int initial_size = queue.size();

        while (!queue.empty()) {
            auto next = queue.front();
            queue.erase(queue.begin());
            auto degree = graph.degree(next);
            initial_size--;
            
            if (years[next] != 2013 && most_similar_degree == -1 || abs(most_similar_degree - my_degree) > abs(my_degree - degree)) {
                most_similar_degree = degree;
                most_similar = next;
            }
            
            if (initial_size <= 0) continue; // only one level away

            auto neighbourhood = graph.neighbours_non_owned(next, IGRAPH_ALL);
            for (auto it2 = neighbourhood.begin(); it2 != neighbourhood.end(); it2++) {
                if (!visited.contains(*it2)) {
                    queue.push_back(*it2);
                    visited.insert(*it2);
                }
            }
        }

        if (most_similar < 0) {
            result.push_back(0);
        } else {
            result.push_back(real_commnunities[most_similar]);
        }
    }
    return result;
}

void exercise_5() {
    auto graph = igraph_wrapper::read_graph_edgelist("../data/aps_2008_2013.adj", false);
    auto items = load_years_and_communities_pajek("../data/aps_2008_2013.net");
    
    // all correct communities
    auto communities         = fmap<pair<int, pair<int, int>>, int>(items, [](auto pair) { return pair.second.second; });

    auto items_2013          = filter<pair<int, pair<int, int>>>(items, [](auto pair) { return pair.second.first == 2013; });

    // list of vertices for which we need predictions
    auto items_2013_vertices = fmap<pair<int, pair<int, int>>, int>(items_2013, [](auto pair) { return pair.first; });
    // correct communities for year 2013
    auto items_2013_comms    = fmap<pair<int, pair<int, int>>, int>(items_2013, [](auto pair) { return pair.second.second; });
    
    // years
    auto years    = fmap<pair<int, pair<int, int>>, int>(items, [](auto pair) { return pair.second.first; });

    auto p1  = predict_baseline(graph, communities, items_2013_vertices);
    auto ca1 = CA(items_2013_comms, p1);
    // cout << ca1 << endl;

    auto p2  = predict_A(graph, communities, items_2013_vertices);
    auto ca2 = CA(items_2013_comms, p2);
    // cout << ca2 << endl;

    auto p3  = predict_B(graph, communities, years, items_2013_vertices);
    auto ca3 = CA(items_2013_comms, p3);
    cout << ca3 << endl;
}

/// DONT FORGET TO INCLUDE GRAPH LIBRARY
/// IN THE FINAL SUBMISSION
int main() {
    // exercise_2();
    // exercise_3();
    exercise_4();
    // exercise_5();
    return 0;
}
